# EASYTICKET

## Description

This _document_ is a list of web services (**ws**) of Easyticket:

* Possibles **JSON** responses
* Error **Responses**

## You can test these ws with Postman 

Download here if you need [Postman](https://www.getpostman.com)
 Login:
 * username: easyticketapps@gmail.com
 * password: _easyticket_

## Google API oauth keys for webapp
* ClientId:  954649540464-l69r33364h9sbtauqche2vnpm9h8qdrh.apps.googleusercontent.com 
* Secret key:  rfQallx0mRkaF4AMgW2mw4b2 


  ## LOGIN/REGISTER WORKFLOW
First proposal about login/register workflow
![](Easyticket.png)
### 1. Login
> **`Works`**

* url: http://easyticketapp.com/dev/api/login.php
* Method: POST
* raw data: "email=`a@a.com`&password=`*********`"
* HTTPHeaders: ["Available-Language" : "`en-EN`" ]
* Success response:
```json 
{
	"data": {
		"type": "login",
		"user": {
		    "id": "1",
			"email": "a@a.com",
			"username" : "albert",
            "auth_token" : "kfkdfj2fd54fda1fd5df1d2fd5fdf1dg2fg2d1f2d",
			"level" : 2,
			"lastvisitDate" : "2017-12-15 12:18:41",
			"language" : "en-GB"
		}
	}
}
```
```xml
<data>
    <type>login</type>
    <user>
        <id>1</id>
        <auth_token>kfkdfj2fd54fda1fd5df1d2fd5fdf1dg2fg2d1f2d</auth_token>
        <username>albert</username>
        <email>a@a.com</email>
        <level>2</level>
        <lastvisitDate>2017-12-15 12:18:41</lastvisitDate>
        <language>es-es</language>
        <registerDate>2017-12-15 12:18:41</registerDate>
    </user>
</data>
```

* Error response Email or Password empty:
```json 
{
	"error": {
		"message": "Email or password empty",
		"error_code": "110"
	}
}
```
```xml
<error>
    <error_code>110</error_code>
    <error_message>Email or password empty</error_message>
</error>
```

* Error response Wrong Email or Password:
```json 
{
	"error": {
		"message": "Wrong email or password try again",
		"error_code": "120"
	}
}
```
```xml
<error>
    <error_code>120</error_code>
    <error_message>Wrong email or password try again</error_message>
</error>
```

* CURL:
```
curl -X POST \
  http://easyticketapp.com/dev/api/login.php \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -d 'email=kim@aficat.com&password=d3fc0n3%23'
```

### 2. Register
> **`Works`**

* url: http://easyticketapp.com/dev/api/register.php
* Method: POST
* raw data: "email=`a@a.com`&password=`******`&password2=`******`"
* HTTPHeaders: ["Available-Language" : "`en-EN`" ]
* Success response:
```json 
{
	"data": {
		"type": "register",
		"id": "1",
		"auth_token" : "kfkdfj2fd54fda1fd5df1d2fd5fdf1dg2fg2d1f2d",
		"user": {
			"email": "a@a.com",
			"name" : "albert",
			"surname" : "vila",
			"created_at" : "2017-11-15 12:18:41",
			"last_visite_date" : "2017-12-15 12:18:41",
			"language" : "en-GB"
		}
	}
}
```

* Error response:
```json 
{
	"error": {
		"message": "Don't match passwords",
		"error_code": "120"
	}
}
```
* CURL:
```
curl -X POST \
  http://easyticketapp.com/dev/api/register.php \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -d 'password=1234&password2=1234'
```
### 3. Re-Send email activation
> `Works`

* url: http://easyticketapp.com/dev/api/resend.php
* Method: POST
* raw data: "email=`a@a.com`"
* HTTPHeaders: ["auth_token" : "`f1d23f15df21fd1f`", "Available-Language" : "`en-EN`" ]
* Success response:
```json 
{
	"data": {
		"type": "account",
		"message": "we just send an email, check your inbox" 
	}
}
```

* Error response:
```json 
{
	"error": {
		"message": "Account doesn't exists",
		"error_code": "121"
	}
}
```
* CURL:
```
curl -X POST \
  http://easyticketapp.com/dev/api/resend.php \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -H 'postman-token: 694595c1-925b-ed57-42f4-8ed300da41b3' \
  -d email=albert.vp%2B1%40gmail.com
```

### 4. Forgot Password
> `Proposal`

* url: http://easyticketapp.com/dev/api/reset.php
* Method: POST
* raw data: "email=`a@a.com`"
* HTTPHeaders: ["Available-Language" : "`en-EN`" ]
* Success response:
```json 
{
	"data": {
		"type": "account",
		"message": "we just send an email to set a new password, check your inbox" 
	}
}
```

* Error response:
```json 
{
	"error": {
		"message": "Reset password email could not be sent",
		"error_code": "140"
	}
}
```
* CURL:
```
curl -X GET \
```

### 5. Is User Logged
> `Proposal`

* url: http://easyticketapp.com/dev/api/`endpoint`.php
* Method: POST
* raw data: "auth_token=`fd5f4f5d4f1f2cx15df5dcv4`"
* HTTPHeaders: ["Available-Language" : "`en-EN`" ]
* Success response:
```json 
{
	"data": {
		"type": "UserLogged",
		"logged":  true or false 
	}
}
```

* Error response:
```json 
{
	"error": {
		"message": "User doesn't have permissions",
		"error_code": "143"
	}
}
```
* CURL:
```
curl -X GET \
```